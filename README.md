# Инструкция тут
https://boosty.to/twinsrpg/posts/e7f98919-3acd-4a82-bf25-862200546765


# 📝 Пример файла module.json


```
{
  "id": "twinsrpg-kftgv",
  "title": "Ключи от Золотого Хранилища",
  "version": "1.1.0",
  "compatibility": {
    "minimum": "11",
    "verified": "11"
  },
  "media": [
    {
      "type": "setup",
      "url": "modules/twinsrpg-kftgv/assets/setup.webp",
      "thumbnail": "modules/twinsrpg-kftgv/assets/setup.webp",
      "loop": false,
      "flags": {}
    }
  ],
  
  "packFolders": [
    {
      "name": "Модули Эфы",
      "sorting": "m",
      "color": "#8a2826",
      "packs": [
        "keys-from-the-golden-vault"
      ],
      "folders": []
    }
  ],
  "styles": [
    "styles/twinsrpg-kftgv.css"
  ],
  "esmodules": [
    "scripts/twinsrpg-kftgv.mjs"
  ],

  "authors": [
    {
      "name": "Evil Efa",
      "url": "https://vk.com/twinsrpg",
      "discord": "eva_efa"
    }
  ],
  "packs": [
    {
      "name": "keys-from-the-golden-vault",
      "label": "Ключи от Золотого Хранилища",
      "banner": "modules/twinsrpg-kftgv/assets/banner.webp",
      "path": "packs/keys-from-the-golden-vault",
      "type": "Adventure",
      "system": "dnd5e",
      "ownership": {
        "PLAYER": "OBSERVER",
        "ASSISTANT": "OWNER"
      }
    }
  ]
}
```

# 📝 Пример оглавления внутри пака-приключения 

```

      "flags": {
        "core": {
          "sheetClass": "twinsrpg-kftgv.KeysFromGoldenVaultJournalSheet"
        },
        "twinsrpg-kftgv": {
          "navigation": {
            "up": "ioHwVM61VFele6PM",
            "next": "HNSt8HeKXeqVdC5d",
            "previous": "ktWSLIBsi2ACgqWB"
          }
        }
      }
```

# 💥 Стили для журналов 

## Нарративный блок

```
<section class="fvtt-relative">
    <section class="fvtt narrative">
        <p>Текст, указанный в подобной рамке, предназначен для прочтения вслух или пересказа своими словами игрокам во время первого посещения локации или при особых обстоятельствах, как указано в тексте приключения.</p>
    </section>
</section>
```
![Пример](images/narrative.png)

## Блок с советами по Foundry 

```
<section class="fvtt advice">
    <figure class="icon">
        <img src="icons/vtt-512.png">
    </figure>
    <article>
        <h3>Активируйте сцену "Музей (день)"</h3>
        <p>Когда ваши персонажи прибудут в музей, не забудьте активировать сцену @UUID[Scene.KGxueWDrz43pDB36]{"Первый Этаж"}</p>
    </article>
</section>
```
![Пример](images/advice.png)

если хотите круглую рамку, то замените
```
<img src="icons/vtt-512.png">
```
на
``` 
<img src="icons/vtt-512.png" class="round">
```
![Пример](images/advice_2.png)
## Таблицы


### Без заголовка
Используется, когда перечисляются факты (например, что может рассказать персонаж)
```
<table class="fvtt">
    <tbody><tr>
            <td>
                <p><strong>Кража яйца.</strong> Персонажам нужно изучить меры безопасности музея, выкрасть Камень Муркмира в подходящий момент (см. «Кража Камня Муркмира» ниже) и доставить его доктору Даннель. Она будет ждать их в переулке между музеем и «Пером мудреца».</p>
            </td>
</tr><tr>
            <td>
                <p><strong>Свойства яйца.</strong> Камень Муркмира нельзя повредить или уничтожить. Раскопка яйца вызвала быстрое развитие существа, и теперь оно готово вот-вот вылупиться.</p>
            </td>
</tr><tr>
            <td>
                <p><strong>Сдерживание яйца.</strong> На данный момент единственный способ остановить вылупление — поместить яйцо в специально сконструированный хрустальный контейнер. Доктор Даннель создала с этой целью особую шкатулку. Кэсси всё еще нужно заделать трещины в хрустале, поэтому герои не могут взять шкатулку с собой на ограбление.</p>
            </td>
</tr><tr>
            <td>
                <p><strong>Что в яйце.</strong> Оккультные источники доктора Даннель указывают на то, что из яйца вылупится чрезвычайно опасное существо, если его не удастся надежно запечатать.</p>
            </td>
</tr></tbody>
</table>
```
![Пример](images/table_1.png)

### Обычная
```
<table class="fvtt">
    <caption><strong>Бродячие чудовища</strong></caption>
    <thead>
        <tr>
            <th>[[/r 1d6]]{1к6}</th>
            <th>Столкновение в Пещерном квартале</th>
            <th>Столкновение в других кварталах</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>2 @UUID[Actor.BxZHe3VhrmRLc3yF]{темных мантии} [darkmantle] преследующих 1 @UUID[Actor.tyPJdRSERLC8mKKi]{грика} [grick]</td>
            <td>3 @UUID[Actor.5ymQpfdIREZitz4K]{заводных защитника} [clockwork defender]</td>
        </tr>
        <tr>
            <td>2-3</td>
            <td>2 @UUID[Actor.tyPJdRSERLC8mKKi]{грика} [grick]</td>
            <td>2 @UUID[Actor.5ymQpfdIREZitz4K]{заводных защитника} [clockwork defender]</td>
        </tr>
        <tr>
            <td>4-6</td>
            <td>1 @UUID[Actor.9HVdj0TGwhHRfrHp]{ползающая насыпь} [shambling mound], состоящая из грибов</td>
            <td>1 @UUID[Actor.4mSxvoZ9hA4Sqv8J]{заводной наблюдатель} [clockwork observer]</td>
        </tr>
    </tbody>
</table>
```
![Пример](images/table_common.png)

### Таблица из "Случайных таблиц"
```
@Embed[RollTable.sr5CLE4PEbBepXS5]{Мистические разряды}
```
![Пример](images/table_random.png)
## Блок с советами (отыгрыш, способности и т.д.)
```
<aside class="notable">
    <h3>Использование заклинаний.</h3>
    <p>Использование заклинаний. Базовой характеристикой древнего ужаса является Интеллект ( [[/save ability=int dc=14]] спасбросок от заклинания). Он может накладывать следующие заклинания, не нуждаясь в компонентах:</p>
    <p><em>2/день каждое: слепота/глухота [blindness/deafness], размытый образ [blur]</em></p>
    <p><em>1/день: проекция [project image]</em></p>
</aside>
```
![Пример](images/aside.png)

## Картинки
### Обычные с подписью

```
<figure>
    <img src="modules/twinsrpg-kftgv/assets/arts/navigation-bar.webp">
    <figcaption>Пример смежных ссылок в заметке @UUID[JournalEntry.ioHwVM61VFele6PM]{Злорадство Муркмира}</figcaption>
</figure>
```
![Пример](images/image-center.png)
### Боковые

- **right/left** (слева/справа)
- **two/three** (масштаб)

```
<figure class="right two">
    <img src="modules/twinsrpg-kftgv/assets/arts/reward.webp">
    <figcaption>Система распределения награды</figcaption>
</figure>
```
![Пример](images/image-right-two.png)

## Вставить иконку
Можно добавить свои, если отредактировать block.css. Например:
```
  & .reference-icon.visibility-state {
    color: #fff;
    background-image: url(../../../icons/svg/cowled.svg);
    margin-bottom: -6px;
  }
```
В html-журнале вставляем вот так:
```
 <span class="reference fa-coins"> </span> 
 ```
### Какие иконки добавлены в blocks.css:
- visibility-state
- secret-door
- locked-door
- fa-map-pin

![Пример](images/icons.png)


